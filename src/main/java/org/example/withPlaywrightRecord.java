package org.example;
import com.microsoft.playwright.*;
import com.microsoft.playwright.options.*;
import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;

import java.nio.file.Paths;
import java.util.*;

public class withPlaywrightRecord {
    public static void main(String[] args) {
        Playwright playwright = Playwright.create();
        Browser browser = playwright
                            .chromium()
                            .launch(new BrowserType.LaunchOptions()
                            .setHeadless(false));
        BrowserContext context = browser.newContext();

        Page page = context.newPage();
        page.navigate("https://www.google.com/");

        page.getByRole(AriaRole.COMBOBOX, new Page.GetByRoleOptions().setName("Buscar")).fill("lionel");
        page.getByRole(AriaRole.OPTION, new Page.GetByRoleOptions().setName("Lionel Messi Futbolista")).locator("div:has-text(\"Futbolista\")").click();
        page.getByRole(AriaRole.LINK, new Page.GetByRoleOptions().setName("Wikipedia")).click();

        assertThat(page).hasURL("https://es.wikipedia.org/wiki/Lionel_Messi");

        page.screenshot(new Page.ScreenshotOptions().setPath(Paths.get("screenshots/" + "example.png")));
        browser.close();
        playwright.close();

    }

}