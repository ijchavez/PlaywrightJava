package org.practice;

import com.microsoft.playwright.options.FilePayload;
import io.thedocs.soyuz.to;
import org.Base.BaseTest;
import org.Utilities.Constants;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class FileUpload extends BaseTest {
    public static final String REACT_PDF = "MODULO I - Unidad 3 Crear una aplicación utilizando el CLI.pdf";
    public static List<String> FILES_TO_UPLOAD = to.list(REACT_PDF, "ejemplo_4.js", "8ffc33eec91be6bec4dd362b51784c99.jpg", "parlantegral216-640-0.jpg");
    List<String> fileList = new ArrayList<String>();
    @Test
    public void uploadFileTest(){
        // https://davidwalsh.name/demo/multiple-file-upload.php
        fileList = page.locator("ul#fileList li").allInnerTexts();
        Assert.assertEquals(fileList.get(0), "No Files Selected");

        page.setInputFiles("input#filesToUpload", Paths.get(Constants.UPLOAD_FOLDER + REACT_PDF));

        fileList = page.locator("ul#fileList li").allInnerTexts();
        Assert.assertEquals(fileList.get(0), REACT_PDF);

        page.setInputFiles("input#filesToUpload", new Path[0]);

        fileList = page.locator("ul#fileList li").allInnerTexts();
        Assert.assertEquals(fileList.get(0), "No Files Selected");

    }
    @Test
    public void uploadMultipleFilesTest(){
        // https://davidwalsh.name/demo/multiple-file-upload.php
        fileList = page.locator("ul#fileList li").allInnerTexts();
        Assert.assertEquals(fileList.get(0), "No Files Selected");

        page.setInputFiles("input#filesToUpload",
                new Path[]{
                        Paths.get(Constants.UPLOAD_FOLDER + FILES_TO_UPLOAD.get(0)),
                        Paths.get(Constants.UPLOAD_FOLDER + FILES_TO_UPLOAD.get(1)),
                        Paths.get(Constants.UPLOAD_FOLDER + FILES_TO_UPLOAD.get(2)),
                        Paths.get(Constants.UPLOAD_FOLDER + FILES_TO_UPLOAD.get(3)),
                });

        fileList = page.locator("ul#fileList li").allInnerTexts();
        fileList.forEach(ele -> System.out.println(ele));

        for(int i = 0; i < fileList.size(); i++){
            Assert.assertEquals(fileList.get(i), FILES_TO_UPLOAD.get(i));

        }

    }
    @Test
    public void uploadNewFileTest(){
        // https://davidwalsh.name/demo/multiple-file-upload.php
        fileList = page.locator("ul#fileList li").allInnerTexts();
        Assert.assertEquals(fileList.get(0), "No Files Selected");

        page.setInputFiles("input#filesToUpload",
                new FilePayload(
                        "test.txt",
                        "text/plain",
                        "this is a text file".getBytes(StandardCharsets.UTF_8))

                );

        fileList = page.locator("ul#fileList li").allInnerTexts();

        fileList.forEach(ele -> System.out.println(ele));
        Assert.assertEquals(fileList.get(0), "test.txt");

    }

}
