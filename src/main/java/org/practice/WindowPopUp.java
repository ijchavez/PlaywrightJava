package org.practice;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import org.Base.BaseTest;
import org.testng.annotations.Test;

public class WindowPopUp extends BaseTest {
    @Test
    public void handleWindowPopUpsTest(){
        page.navigate("https://ijchavez.github.io/react-portfolio-website/");

        Page popup = page.waitForPopup(() ->{
           Locator github = page.locator("a[href='https://www.github.com/ijchavez']").first();
           github.click();

        });
        System.out.println(popup.title());
        popup.close();

        System.out.println(page.title());

    }
    @Test
    public void handleTabsTest(){
        page.navigate("https://ijchavez.github.io/react-portfolio-website/");

        Page popup = page.waitForPopup(() ->{
            page.click("a[target='_blank']");

        });
        popup.waitForLoadState();
        popup.navigate("https://flight-information.vercel.app/");

        System.out.println(popup.title());
        popup.close();

        System.out.println(page.title());

    }

}
