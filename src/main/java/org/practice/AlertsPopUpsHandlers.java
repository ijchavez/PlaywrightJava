package org.practice;

import org.Base.BaseTest;
import org.Utilities.Constants;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AlertsPopUpsHandlers extends BaseTest {
    @Test
    public void alertHandlers(){
        page.click("//button[text()='Click for JS Alert']");

        String result = page.textContent("#result");
        System.out.println(result);

        Assert.assertEquals(result, "You successfully clicked an alert");

    }
    @Test
    public void confirmationCancelHandlers(){
        page.click("//button[text()='Click for JS Confirm']");

        String result = page.textContent("#result");
        System.out.println(result);

        Assert.assertEquals(result, "You clicked: Cancel");

    }
    @Test
    public void confirmationAcceptHandlers(){
        page.onDialog(dialog -> {
            String message = dialog.message();
            System.out.println(message);

            Assert.assertEquals(message, "I am a JS Confirm");
            dialog.accept();

        });
        page.click("//button[text()='Click for JS Confirm']");

        String result = page.textContent("#result");
        System.out.println(result);

        Assert.assertEquals(result, "You clicked: Ok");

    }
    @Test
    public void confirmationPromptHandlers(){
        page.onDialog(dialog -> {
            String message = dialog.message();
            System.out.println(message);

            Assert.assertEquals(message, "I am a JS prompt");
            dialog.accept(Constants.AUTOMATIONPRACTICE_URL);

        });
        page.click("//button[text()='Click for JS Prompt']");

        String result = page.textContent("#result");
        System.out.println(result);

        Assert.assertEquals(result, "You entered: " + Constants.AUTOMATIONPRACTICE_URL);

    }

}
