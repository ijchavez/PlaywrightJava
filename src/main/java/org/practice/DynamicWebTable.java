package org.practice;

import com.microsoft.playwright.Locator;
import org.Base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DynamicWebTable extends BaseTest {
    @Test
    public void dynamicWebtable(){
        // using datatables.net
        Locator table = page.locator("table#example tr");
        Locator row = table.locator(":scope", new Locator.LocatorOptions().setHasText("Ashton Cox"));
        Locator checkbox = row.locator(".select-checkbox");

        checkbox.click();

        row.allInnerTexts().forEach(e -> System.out.println(e));
        table.locator(":scope").allInnerTexts().forEach(e -> System.out.println(e));

    }
    @Test
    public void dynamicWebtableAng(){
        // https://www.primefaces.org/primeng/table
        page.navigate("https://www.primefaces.org/primeng/table");

        Locator table = page.locator("table#pr_id_2-table tr");
        Locator row = table.locator(":scope", new Locator.LocatorOptions()
                                                            .setHasText("James Butt"))
                                                            .locator(".p-checkbox-box");
        row.click();

        table.locator(":scope").allInnerTexts().forEach(e -> System.out.println(e));
        Assert.assertTrue(row.isChecked());

    }

}
