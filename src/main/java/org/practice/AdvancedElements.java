package org.practice;

import com.microsoft.playwright.Locator;
import org.Base.BaseTest;
import org.testng.annotations.Test;

public class AdvancedElements extends BaseTest {
    @Test
    public void advEl(){
        // using automationpractice
        Locator div = page.locator("div");
        Locator p = div.locator("p");

        System.out.println(p.count());

    }
    @Test
    public void advElemOrange(){
        page.navigate("https://www.orangehrm.com/hris-hr-software-demo/");

        page.mouse().down();
        page.mouse().move(0, 500);

        Locator country = page.locator("select#Form_getForm_Country:has(option[value='India'])");
        country.allInnerTexts().forEach(e -> System.out.println(e));

    }
    @Test
    public void advElemAmazon(){
        page.navigate("https://www.amazon.com/");

        Locator footerList = page.locator("div.navFooterLinkCol:has(a[href='https://www.amazon.jobs'])");
        footerList.allInnerTexts().forEach(e -> System.out.println(e));

    }
}
