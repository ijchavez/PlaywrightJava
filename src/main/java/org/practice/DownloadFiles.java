package org.practice;

import com.microsoft.playwright.Download;
import org.Base.BaseTest;
import org.testng.annotations.Test;

import java.nio.file.Paths;

public class DownloadFiles extends BaseTest {
    @Test
    public void downloadFileTest(){
        page.navigate("https://chromedriver.storage.googleapis.com/index.html?path=108.0.5359.22/");

        Download download = page.waitForDownload(() -> {
            page.click("a:text('chromedriver_mac64.zip')");

        });
        System.out.println(download.url());
        System.out.println(download.path().toString());

        download.saveAs(Paths.get("chromedriver.zip"));

    }

}
