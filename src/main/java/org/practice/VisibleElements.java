package org.practice;

import org.Base.BaseTest;
import org.testng.annotations.Test;

import java.util.List;

public class VisibleElements extends BaseTest {
    @Test
    public void iframeByXpath(){
        // using automationpractice
        List<String> visibleAnchors = page.locator("a:visible").allInnerTexts();
        for(String e: visibleAnchors){
            System.out.println(e);

        }

    }

}
