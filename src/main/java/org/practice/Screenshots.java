package org.practice;

import org.Base.BaseTest;
import org.testng.annotations.Test;

public class Screenshots extends BaseTest {
    @Test
    public void screenshotTest(){
        page.navigate("https://ecommerce-sanity-stripe-jsm.vercel.app/");

        getScreenShot(page,true, "screenshotpagefs.png");
        getScreenShot(page, false, "screenshotpage.png");

        getLocatorScreenShot(page, "div[class='hero-banner-container']", "banner.png");

    }

}
