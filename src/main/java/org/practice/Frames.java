package org.practice;

import com.microsoft.playwright.FrameLocator;
import com.microsoft.playwright.Locator;
import org.Base.BaseTest;
import org.testng.annotations.Test;

public class Frames extends BaseTest {
    //this is used with https://academy.naveenautomationlabs.com/
    @Test
    public void frameByXpath(){
        String header = page.frameLocator("frame[name='main']")
                            .locator("h2")
                            .textContent();
        // "//frame[@name='main']" -> this is the same as above
        System.out.println(header);

    }
    @Test
    public void frameByFrameName(){

        String h2 = page.frame("main")
                .locator("h2")
                .textContent();
        System.out.println(h2);

    }
    @Test
    public void iframeByXpath(){
        page.navigate("https://csreis.github.io/tests/cross-site-iframe.html");

        Locator complexPageBtn = page.locator("text=Go cross-site (complex page)");
        System.out.println(complexPageBtn.textContent());

        complexPageBtn.click();

        FrameLocator frmlctr = page.frameLocator("//iframe[contains(@id, 'frame1')]");
        String title = frmlctr.locator("//*[@class='console-title']").textContent();

        System.out.println(title);

    }
}
