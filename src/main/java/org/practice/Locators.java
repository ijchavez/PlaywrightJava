package org.practice;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.assertions.PlaywrightAssertions;
import org.Base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;


public class Locators extends BaseTest {
    //url used in BaseTest https://www.orangehrm.com/
    public void clickOnContactSalesBtn(){
        Locator contactSalesBtn = page.locator("role=button[name='Contact Sales']");
        contactSalesBtn.click();

    }
    @Test
    public void oneLocator(){
        clickOnContactSalesBtn();

        PlaywrightAssertions.assertThat(page).hasURL("https://www.orangehrm.com/contact-sales/");
        Assert.assertEquals("https://www.orangehrm.com/contact-sales/", page.url());

    }
    @Test
    public void multipleLocators(){
        clickOnContactSalesBtn();

        page.mouse().down();
        page.mouse().move(0, 500);

        Locator country = page.locator("select#Form_getForm_Country option");
        System.out.println(country.count());

        /*
        for(int i = 0; i < country.count(); i++){
          String text = country.nth(i).textContent();
          System.out.println(text);

        }
        */
        List<String> countryList = country.allInnerTexts();
        for(String countryFromList : countryList){
            System.out.println(countryFromList);

        }
        System.out.println("***************************");
        countryList.forEach(ele -> System.out.println(ele));

    }


}
