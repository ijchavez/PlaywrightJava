package org.practice;

import com.microsoft.playwright.Locator;
import org.Base.BaseTest;
import org.testng.annotations.Test;

import java.util.List;

public class Selectors extends BaseTest {
    @Test
    public void textLocator(){
        //this is used with automation practice url
        Locator contactUsBtn = page.locator("text=Contact us").first();
        contactUsBtn.click();

    }
    @Test
    public void otherTextLocator(){
        //this is used with https://academy.naveenautomationlabs.com/
        Locator loginBtn = page.locator("text = Login");
        System.out.println(loginBtn.count());

        loginBtn.first().click();

    }
    @Test
    public void multipleElementLocator(){
        //this is used with https://academy.naveenautomationlabs.com/
        Locator loginBtn = page.locator("text = Login");
        System.out.println(loginBtn.count());

        List<String> loginListText = loginBtn.allTextContents();
        for(String text: loginListText){
            System.out.println(text);

        }

    }

}
