package org.Utilities;

import org.Pages.BasePage;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Utilities extends BasePage {
    public Utilities(){


    }
    /*
    * This method is used tu initialize the properties from config_file
    * */
    public Properties init_prop() throws IOException {
        FileInputStream ip = new FileInputStream("./src/test/resources/config/config.properties");

        prop = new Properties();
        prop.load(ip);

        return prop;

    }

}
