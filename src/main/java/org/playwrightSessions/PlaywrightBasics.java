package org.playwrightSessions;

import com.microsoft.playwright.*;
import com.microsoft.playwright.BrowserType.LaunchOptions;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.nio.file.Paths;

public class PlaywrightBasics {
    static Playwright playwright;
    static LaunchOptions lp;
    static Browser browser;
    static BrowserContext context;
    static Page page;

    public static Page setPage(String aChannel, boolean isHeadless){
        playwright = Playwright.create();

        lp = new BrowserType.LaunchOptions();
        lp.setChannel(aChannel);
        lp.setHeadless(isHeadless);

        setNavigator(aChannel);

        context = browser.newContext();
        startTracing(context, true, true, false );

        page = context.newPage();
        return page;

    }
    public static void setNavigator(String aChannel){
        if(aChannel.equals("msedge") || aChannel.equals("chrome")){
            browser = playwright.chromium().launch(lp);

        }
        if(aChannel.equals("firefox")){
            browser = playwright.firefox().launch(lp);

        }
        if(aChannel.equals("webkit")){
            browser = playwright.webkit().launch(lp);

        }

    }
    public static void startTracing(BrowserContext aContext, boolean setScreenshot, boolean setSnapshot, boolean setSources){
        aContext.tracing().start(new Tracing.StartOptions()
                .setScreenshots(setScreenshot)
                .setSnapshots(setSnapshot)
                .setSources(setSources));

    }
    @BeforeMethod
    public static void setUp(){
        page = setPage("chrome", false);
        page.navigate("https://www.amazon.com");

    }
    @Test
    public void firsTest(){
        String pageTitle = page.title();
        System.out.println(pageTitle);

        String url = page.url();
        System.out.println(url);

    }
    @AfterMethod
    public void tearDown(){
        stopTracing(context, "trace/" + "trace.zip");

        browser.close();
        playwright.close();

    }
    public void stopTracing (BrowserContext aContext, String traceName){
        aContext.tracing().stop(new Tracing.StopOptions()
                .setPath(Paths.get(traceName)));

    }
    
}
