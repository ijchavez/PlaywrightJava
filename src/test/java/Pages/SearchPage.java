package Pages;

import com.microsoft.playwright.Page;

public class SearchPage extends HomePage{
    public SearchPage(Page p) {
        super(p);

    }
    private String searchHeader = "div#content h1";
    public String getSearchHeader(){
        return page.locator(searchHeader).innerText();

    }

}
