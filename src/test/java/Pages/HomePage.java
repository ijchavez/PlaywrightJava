package Pages;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;

public class HomePage extends BasePage{
    public HomePage(Page p){
        page = p;

    }
    private String search = "input[name='search']";
    private String searchIcon = "div#search button";
    private String myAccountBtn = "//span[contains(text(),'My Account')]";
    private String loginLink = "a:text('Login')";
    private String registerLink = "a:text('Register')";

    public void fillSearch(String productName){
        page.fill(search, productName);

    }
    public SearchPage clickOnSearchBtn(){
        page.click(searchIcon);

        searchPage = new SearchPage(page);
        return searchPage;

    }
    public void clickOnMyAccountBtn(){
        page.locator(myAccountBtn).click();

    }
    public LoginPage clickOnLoginPageBtn(){
        page.click(loginLink);

        loginPage = new LoginPage(page);
        return loginPage;

    }
    public RegisterPage clickOnRegisterPageBtn(){
        page.click(loginLink);

        registerPage = new RegisterPage(page);
        return registerPage;

    }

}
