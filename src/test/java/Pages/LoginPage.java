package Pages;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;

public class LoginPage extends HomePage{
    public LoginPage(Page p) {
        super(p);

    }
    private final String emailInput = "input#input-email";
    private final String passwordInput = "input#input-password";
    private final String loginBtn = "//input[@value='Login']";
    private final String registerBtn = "//a[@class='btn btn-primary']";
    private final String forgottenPasswordLink = "//div[@class='form-group']//a[normalize-space()='Forgotten Password']";
    private final String incorrectPasswordErrorMessage = "//div[@class='alert alert-danger alert-dismissible']";
    public boolean isForgottenPasswordLinkVisible(){
        return page.isVisible(forgottenPasswordLink);

    }
    public Locator getIncorrectPasswordErrorMessage(){
        return page.locator(incorrectPasswordErrorMessage);

    }
    public RegisterPage clickOnContinueBtn(){
        page.click(registerBtn);

        registerPage = new RegisterPage(page);
        return registerPage;

    }
    public MyAccountPage doLogin(String username, String password){
        page.fill(emailInput, username);
        page.fill(passwordInput, password);

        page.click(loginBtn);

        myAccountPage = new MyAccountPage(page);
        return myAccountPage;

    }

}
