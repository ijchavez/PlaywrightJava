package Pages;

import Utilities.*;
import com.microsoft.playwright.*;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Properties;

public class BasePage {
    public static Playwright playwright;
    public static BrowserType.LaunchOptions launchOptions;
    public static Browser browser;
    public static BrowserContext browserContext;
    public static Page page;
    public static Properties prop;
    public HomePage homePage;
    public SearchPage searchPage;
    public LoginPage loginPage;
    public MyAccountPage myAccountPage;
    public RegisterPage registerPage;
    public static Utilities utils;

    public static Page setPage(String aChannel, boolean isHeadless) throws IOException {
        playwright = Playwright.create();

        launchOptions = new BrowserType.LaunchOptions();
        setNavigator(aChannel, isHeadless, launchOptions);

        if(aChannel.equals("webkit")){
            browserContext = browser.newContext(setViewPort(1920, 1080));

        }else{
            browserContext = browser.newContext(setViewPort(0, 0));

        }
        startTracing(browserContext, true, true, false );

        page = browserContext.newPage();
        return page;

    }

    public static void setNavigator(String aChannel, boolean isHeadless, BrowserType.LaunchOptions lp){
        lp.setChannel(aChannel);
        lp.setHeadless(isHeadless);

        if(!aChannel.equals("webkit")){
            lp.setArgs(Constants.BROWSER_ARGS);
            if(aChannel.equals("msedge") || aChannel.equals("chrome")){
                browser = playwright.chromium().launch(lp);

            }
            if(aChannel.equals("firefox")){
                browser = playwright.firefox().launch(lp);

            }

        }else{
            browser = playwright.webkit().launch(lp);

        }

    }
    public static Browser.NewContextOptions setViewPort(int x, int y){
        Browser.NewContextOptions bnco;
        if(x == 0 && y == 0){
            bnco = new Browser.NewContextOptions().setViewportSize(null);

        }else{
            bnco = new Browser.NewContextOptions().setViewportSize(x, y);
        }
        return bnco;

    }
    public void close(){
        page.close();
        browser.close();
        playwright.close();

    }
    public static void startTracing(BrowserContext aContext, boolean setScreenshot, boolean setSnapshot, boolean setSources){
        aContext.tracing().start(new Tracing.StartOptions()
                .setScreenshots(setScreenshot)
                .setSnapshots(setSnapshot)
                .setSources(setSources));

    }
    public void stopTracing (BrowserContext aContext, String traceName){
        aContext.tracing().stop(new Tracing.StopOptions()
                .setPath(Paths.get(traceName)));

    }
    public static void setProps() throws IOException {
        utils = new Utilities();
        prop = utils.init_prop();

    }
    public static Browser.NewContextOptions setRecordingVideo() throws IOException {
        setProps();
        return new Browser.NewContextOptions()
                .setRecordVideoDir(Paths.get(prop.getProperty("videofolder")));

    }
    public static Browser.NewContextOptions setRecordingVideo(int x, int y) throws IOException {
        setProps();
        return new Browser.NewContextOptions()
                .setRecordVideoDir(Paths.get(prop.getProperty("videofolder")))
                .setRecordVideoSize(x, y);

    }
    public static String getScreenShot(Page aPage, boolean fullPage, String filename){
        byte[] screenshot = aPage.screenshot(new Page.ScreenshotOptions()
                                 .setPath(Paths.get(prop.getProperty("screenshotfolder") + filename))
                                 .setFullPage(fullPage));

        String base64Path = Base64.getEncoder().encodeToString(screenshot);
        return base64Path;

    }
    public static void getLocatorScreenShot(Page aPage, String aSelector, String filename){
        aPage.locator(aSelector)
             .screenshot(new Locator.ScreenshotOptions()
                                    .setPath(Paths.get(prop.getProperty("screenshotfolder") + filename)));

    }
    public HomePage startTest(){
        homePage = new HomePage(page);
        return homePage;

    }

}
