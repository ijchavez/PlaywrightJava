package Pages;

import com.microsoft.playwright.Page;

public class MyAccountPage extends LoginPage{
    public MyAccountPage(Page p) {
        super(p);

    }
    private final String myAccountTitle = "//h2[normalize-space()='My Account']";
    public boolean isMyAccountTitleVisible(){
        return page.isVisible(myAccountTitle);

    }

}
