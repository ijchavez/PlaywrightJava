package Utilities;

import io.thedocs.soyuz.to;

import java.util.List;

public class Constants {
    public static final String ORANGEHRM_URL = "http://automationpractice.com/index.php";
    public static final String AUTOMATIONPRACTICE_URL = "http://automationpractice.com/index.php";
    public static final String ACADEMY_NAVEENAUTOMATIONLABS_URL = "https://academy.naveenautomationlabs.com/";
    public static final String NAVEENAUTOMATIONLABS_OPENCART_URL = "https://naveenautomationlabs.com/opencart/";
    public static final String LONDONFREELANCEFRAMES_URL = "http://www.londonfreelance.org/courses/frames/index.html";
    public static final String DATATABLES_URL = "https://datatables.net/extensions/select/examples/initialisation/checkbox";
    public static final String HEROKUAPP_URL = "http://the-internet.herokuapp.com";
    public static final String HEROKUJSALERTS_PATH = "/javascript_alerts";
    public static final String FILE_UPLOADS_URL = "https://davidwalsh.name/demo/multiple-file-upload.php";
    public static final String UPLOAD_FOLDER = "filesToUpload/";
    public static final String SEARCH = "Search - ";
    public static final List<String> INCORRECT_LOGIN_DATA = to.list("nonexistemail@mail.com", "wrongpassword");
    public static final String INCORRECT_CREDENTIALS_ERROR_MESSAGE = " Warning: No match for E-Mail Address and/or Password.";
    public static final List<String> BROWSER_ARGS = to.list("--start-maximized");

}
