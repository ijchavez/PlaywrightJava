package extentReport;

import Base.BaseTest;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import org.testng.Reporter;
import java.util.HashMap;
import java.util.Map;

public class ExtentTestManager extends BaseTest {

   static Map<Integer, ExtentTest> extentTestMap = new HashMap<Integer, ExtentTest>();
   public static ExtentReports extent = ExtentManager.getInstance();
   private static final ThreadLocal<String> categoryName = new ThreadLocal<String>();
   
   public static String HTML_BREAK = "<br/>"; 
   
   public static synchronized ExtentTest getTest() {
       return extentTestMap.get((int) Thread.currentThread().getId());
       
   }
   public static ExtentReports getExtent() {
	   return extent;
	   
   }
   public static synchronized void endTest() {
       extent.flush();
       
   }
   public synchronized static void createTest(String testName, String description) {
       extentTestMap.put((int) Thread.currentThread().getId(), extent.createTest(testName, description));
       
   }
   public static ThreadLocal<String> getCategoryName() {
       return categoryName;
       
   }
   public static void setCategoryName(String categoryName) {
       getCategoryName().set(categoryName);
       
   }
   public synchronized static void getLog(String aLog) {
	   Reporter.log(aLog + HTML_BREAK);
	   
   }
   public synchronized static void reporterLog(String log) {
           if (ExtentTestManager.getTest() != null) {
               ExtentTestManager.getTest().log(Status.INFO, log);
               getLog(log);
               
           }
           
   }
   public synchronized static void reporterLog(String log, Status aStatus) {
       if (ExtentTestManager.getTest() != null) {
           ExtentTestManager.getTest().log(aStatus, log);
           getLog(log);
           
       }
       
   }
   public synchronized static void reporterJsonLog(String aLog, String aNodeTitle) {
	   ExtentTestManager.getTest().createNode(aNodeTitle).info(aLog);
       
   }

}