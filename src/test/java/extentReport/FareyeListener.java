package extentReport;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import static Pages.BasePage.getScreenShot;
import static Pages.BasePage.page;
import static extentReport.ExtentManager.*;

public class FareyeListener implements ITestListener {
	
	public static String THREE_DASH = " --- ";
	public static String UNDERSCORE = "_";
	
	public static String START_STRING = "===== ";
	public static String FINISH_STRING = " =====";
	
	public static String TEST_SUITE = "Test suite ";
	public static String STARTED = "started ";
	public static String FINISHED = "finished ";
	public static String EXECUTING = "Executing : ";
	
	public static String SUCCESS = "Finish sucessfully: ";
	public static String ERRORS = "Finish with errors: ";
	
	public static String OK = "Test finished successfully";
	public static String SKIPPED = "This test was skipped: ";
	public static String FAILED = " Test Failed: ";
	
	public static String FONT_HTML_SUCCESS = "<br><font color= green>";
	public static String FONT_HTML_FAIL = "<br><font color= red>";
	public static String IMAGE_HTML = "Image: ";
	public static String FONT_CLOSE = "</font></b>";

    private static long endTime;
    private static void setStartTime(long startTime) {						
    	
    }
    private static void setEndTime(long endTime) {
        FareyeListener.endTime = endTime;
        
    }
    public synchronized static long getReportName() {
        return endTime;
        
    }
    public synchronized void onStart(ITestContext context) {
    	String outPutDirectory = context.getOutputDirectory();
    	System.out.println(TEST_SUITE + STARTED + outPutDirectory + THREE_DASH + context.getName());
    	setReportName(context.getName());
    	
    }
    public synchronized void onFinish(ITestContext context) {
        setStartTime(context.getStartDate().getTime());
        setEndTime(context.getEndDate().getTime());
        
        System.out.println(TEST_SUITE + FINISHED + context.getOutputDirectory() + THREE_DASH + context.getName());
        
        
    }
    public synchronized void onTestStart(ITestResult result) {
        System.out.println(START_STRING + EXECUTING + getSimpleMethodName(result) + FINISH_STRING);
        
        ExtentTestManager.createTest(result.getName(),result.getMethod().getDescription());
        ExtentTestManager.setCategoryName(getSimpleClassName(result));
        
    }
    public synchronized void onTestSuccess(ITestResult result) {
        System.out.println(START_STRING + SUCCESS + getSimpleMethodName(result) + FINISH_STRING);
        
        ExtentTestManager.getTest().assignCategory(getSimpleClassName(result));
        ExtentTestManager.getTest().pass(FONT_HTML_SUCCESS + IMAGE_HTML + FONT_CLOSE,
                                        MediaEntityBuilder.createScreenCaptureFromBase64String(
                                                getScreenShot(page,true, "screenshotpagefs.png"),
                                                result.getMethod().getMethodName()).build());
        
        addExtentLabelToTest(result);
        ExtentTestManager.endTest();
        
    }
    public synchronized void onTestFailure(ITestResult result) {
        System.out.println(START_STRING + ERRORS + getSimpleMethodName(result) + FINISH_STRING);
        
        ExtentTestManager.getTest().assignCategory(getSimpleClassName(result));
        ExtentTestManager.getTest().log(Status.FAIL, result.getName() + FAILED + result.getThrowable());
        
        ExtentTestManager.getTest().fail(FONT_HTML_FAIL + IMAGE_HTML + FONT_CLOSE,
                                        MediaEntityBuilder.createScreenCaptureFromBase64String(
                                                getScreenShot(page,true, "screenshotpagefs.png"),
                                                result.getMethod().getMethodName()).build());
        
        addExtentLabelToTest(result);
        ExtentTestManager.endTest();
        
    }
    public synchronized void onTestSkipped(ITestResult result) {
        System.out.println(START_STRING + SKIPPED + getSimpleMethodName(result) + FINISH_STRING);
        
        ExtentTestManager.getTest().log(Status.SKIP, result.getName() + " " + SKIPPED +  result.getThrowable());
        
        addExtentLabelToTest(result);
        ExtentTestManager.endTest();
        
    }
    public synchronized void onTestFailedButWithinSuccessPercentage(ITestResult result) {
    	
    }
    private synchronized String getSimpleClassName(ITestResult result) {
        return result.getMethod().getRealClass().getSimpleName();
        
    }
    private synchronized String getSimpleMethodName(ITestResult result) {
        return result.getName();

    }
    private synchronized void addExtentLabelToTest(ITestResult result) {
        if (result.getStatus() == ITestResult.SUCCESS)
            ExtentTestManager.getTest().pass(MarkupHelper.createLabel(OK, ExtentColor.GREEN));
        
        else if (result.getStatus() == ITestResult.FAILURE) {
            ExtentTestManager.getTest().fail(MarkupHelper.createLabel(FAILED, ExtentColor.RED));
            
        } else if(result.getStatus() == ITestResult.SKIP) {
        	ExtentTestManager.getTest().skip(MarkupHelper.createLabel(SKIPPED, ExtentColor.ORANGE));
        
        }
        
    }

}