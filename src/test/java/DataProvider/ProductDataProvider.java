package DataProvider;

import org.testng.annotations.DataProvider;
public class ProductDataProvider {
    @DataProvider
    public static Object[][] getProductData() {
        return new Object[][]{
                {"MacBook"},
                {"iPhone"},
                {"Apple Cinema 30\""},
                {"Canon EOS 5D"}

        };

    }
}
