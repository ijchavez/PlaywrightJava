package Test;

import Base.BaseTest;
import Utilities.Constants;
import DataProvider.ProductDataProvider;

import extentReport.FareyeListener;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.microsoft.playwright.Locator;

@Listeners(FareyeListener.class)
public class OpenCart extends BaseTest {
    @Test(dataProvider = "getProductData", dataProviderClass = ProductDataProvider.class)
    public void seachTest(String productName){
        homePage = startTest();
        homePage.fillSearch(productName);

        searchPage = homePage.clickOnSearchBtn();

        String searchText = searchPage.getSearchHeader();
        System.out.println(searchText);

        Assert.assertEquals(searchText, Constants.SEARCH + productName,
                            "error: " + searchText + " is not equal to " + Constants.SEARCH + productName);

    }
    @Test
    public void forgottenPasswordLinkVisibilityTest(){
        homePage = startTest();
        homePage.clickOnMyAccountBtn();

        loginPage = homePage.clickOnLoginPageBtn();

        boolean isForgottenPasswordVisible = loginPage.isForgottenPasswordLinkVisible();
        Assert.assertTrue(isForgottenPasswordVisible, "Forgotten Password link is not visible");

    }
    @Test
    public void correctLoginTest() {
        homePage = startTest();
        homePage.clickOnMyAccountBtn();

        loginPage = homePage.clickOnLoginPageBtn();
        myAccountPage = loginPage.doLogin(prop.getProperty("username").trim(),
                                          prop.getProperty("password").trim());

        boolean myAccountTitleVisible = myAccountPage.isMyAccountTitleVisible();
        Assert.assertTrue(myAccountTitleVisible, "My Account title is not visible");

    }
    int incorrectLoginTest = -1;
    @Test(invocationCount = 2)
    public void incorrectLoginTest() {
        incorrectLoginTest++;

        homePage = startTest();
        homePage.clickOnMyAccountBtn();

        loginPage = homePage.clickOnLoginPageBtn();
        if(incorrectLoginTest == 0){
            loginPage.doLogin(Constants.INCORRECT_LOGIN_DATA.get(incorrectLoginTest),
                                     prop.getProperty("password").trim());

        }else{
            loginPage.doLogin(prop.getProperty("username").trim(),
                                     Constants.INCORRECT_LOGIN_DATA.get(incorrectLoginTest));

        }

        Locator incorrectCredentialsErrorMessage = loginPage.getIncorrectPasswordErrorMessage();
        Assert.assertEquals(incorrectCredentialsErrorMessage.innerText(), Constants.INCORRECT_CREDENTIALS_ERROR_MESSAGE);

    }

}
