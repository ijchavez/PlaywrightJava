package Base;

import Pages.BasePage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import java.io.IOException;

public class BaseTest extends BasePage {
    @Parameters({ "browser" })
    @BeforeMethod
    public static void setUp(String browserName) throws IOException {
        setProps();

        page = setPage(browserName, Boolean.parseBoolean(prop.getProperty("headless")));
        page.navigate(prop.getProperty("url").trim());

    }
    @AfterMethod
    public void tearDown() throws IOException {
        setProps();
        stopTracing(browserContext, prop.getProperty("tracefolder") + "trace.zip");

        close();

    }

}
